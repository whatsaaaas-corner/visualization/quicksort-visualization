# quicksort-visualization

- Quicksort is an O(n log n) efficient sorting algorithm, serving as a systematic method for placing the elements of a random access file or an array in order. ([wiki](https://en.wikipedia.org/wiki/Quicksort))
- This repo represents visualization of the quick sort

**DEMO**

![DEMO](https://media.giphy.com/media/bLlPVkXAG9sOnJtgjq/giphy.gif)
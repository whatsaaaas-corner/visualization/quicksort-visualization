/*
    Classic implementation of quicksort
    Author: @whatsaaaa
*/

let actions = [];
let pivots = [];
let height = 0;

async function quickSort(array, start, end) {
    if (start >= end) return;

    start = start || 0;
    end = end || array.length - 1;

    let pivot = await partition(array, start, end);

    Promise.all([quickSort(array, start, pivot - 1),  await quickSort(array, pivot, end)])

    pivots.splice(pivots.indexOf(pivot), 1);

    return array;
}

async function swap(array, swapIndex, index) {
    await wait(250);
    var t = array[swapIndex];
    array[swapIndex] = array[index];
    array[index] = t;
}

async function partition(array, start, end) {
    let pivot = end;
    let swapIndex = start;
    pivots.push(swapIndex)

    for (var i = start; i < end; i++) {
        if (array[i] <= array[pivot]) {
            await swap(array, swapIndex, i);
            pivots.splice(pivots.indexOf(swapIndex), 1);
            swapIndex = swapIndex + 1;
            pivots.push(swapIndex)
        }
    }

    await swap(array, swapIndex, i);

    return swapIndex;
}

function commitAction(action, start, end) {
    actions.push({
        action: action,
        start: start,
        end: end
    });
}

function visualize(array) {
    for (let i = 0; i < array.length; i++) {
        setupArray(array);
    }
}

function setupArray(array) {
    let len = array.length;
    let element = document.querySelector('#scene');
    
    if (len == 0) return;

    let html = generateHTML(array);
    element.innerHTML = html;
}

function generateHTML(array) {
    if (pivots.length == 0) clearInterval(intervalId);

    let html = '';
    for (let i = 0; i < array.length; i++) {
        height = 10 * array[i];
        if (pivots.indexOf(i) > -1) {
            html += `
                <span style='background-color:red; height:${height}px'>${array[i]}</span>
            `;
        }
        else {
            html += `
            <span style='height:${height}px'>${array[i]}</span>
            `;
        }
        
    }

    return html;
}


function wait(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}